var express = require('express');
var path = require('path');
var babel = require("babel/register");

var app = express();
var port = 8080;

app.use(express.static(path.join(__dirname, 'public')));
app.set('views', path.join(__dirname, 'app/views'));
app.set('view engine', 'ejs');

var React = require('react');
var App = React.createFactory(require('./app/components/App'));
app.get('/', function(req, res){
	res.render('index.ejs', {reactOutput: React.renderToString(App({}))});
});

app.listen(port);

console.log('Server is listening on port ' + port);
