import React from 'react';
import config from '../config/config.json';
import ChannelEngine from '../data/ChannelEngine';
import OrderListItem from './OrderListItem';

var OrderList = React.createClass({
	getInitialState() {
		return {
			orders: []
		};
	},
	componentDidMount() {
		var api = new ChannelEngine(config.tenant, config.apiKey, config.apiSecret);
		api.getOrders([0], (data) => {
			if(this.isMounted()) {
				this.setState({
					orders: data
				});
			}
		});
	},
	render() {
		var items = this.state.orders.map((order, i) => <OrderListItem key={i} order={order} />);

		return (
			<div className="orderlist">
				{items}
			</div>
		);
	}
});

export default OrderList;