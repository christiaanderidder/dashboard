import React from 'react';

var OrderListItem = React.createClass({
	getInitialState() {
		return {
			order: null
		};
	},
	componentDidMount() {
		
	},
	render() {
		return (
			<div className="orderlist--item">
				#{this.props.order.Id}: &euro;{this.props.order.TotalInclVat}
			</div>
		);
	}
});

export default OrderListItem;