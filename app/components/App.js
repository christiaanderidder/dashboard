import React from 'react';
import OrderList from './OrderList';

var App = React.createClass({
	render() {
		return (
			<div id="main">
				<OrderList />
			</div>
		);
	}
});

export default App