var gulp = require('gulp');
var sass = require('gulp-sass');
var watch = require('gulp-watch');
var stream = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var browserify = require('browserify');
var babelify = require('babelify');

gulp.task('scripts', function () {

	// https://github.com/gulpjs/gulp/blob/master/docs/recipes/browserify-transforms.md
	// https://babeljs.io/docs/setup/#browserify
	browserify({
		entries: './browser.js',
		debug: true,
		transform: [babelify]
	})
	.bundle()
	.pipe(stream('app.js'))
	.pipe(buffer())
	.pipe(gulp.dest('./public/'));

});

gulp.task('styles', function () {

	gulp.src('./app/assets/app.scss')
	.pipe(sass())
	.pipe(gulp.dest('./public/'));

});

gulp.task('default', ['build'], function(){
	gulp.watch('./app/**/*.js', ['scripts']);
	gulp.watch('./app/assets/**/*.scss', ['styles']);
});

gulp.task('build', ['scripts', 'styles']);